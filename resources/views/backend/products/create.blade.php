<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Add Product
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Products </x-slot>

            <li class="breadcrumb-item active">Add New</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>


    <div class="card shadow-lg border-0 rounded-lg mt-5">
       
            <div class="card-header">
                <i class="fas fa-table me-1"></i> 
                Products <a class="btn btn-primary" href="{{ route('products.index') }}">List</a>
            </div>
            

       
        <div class="card-body">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

            <form action="{{ route('products.store') }}" method="POST">
                @csrf
                <div class="form-floating mb-3 mb-md-0">
                    <input class="form-control" name="title" id="inputTitle" type="text" placeholder="Enter Title" value="{{ old('title')}}">
                    <label for="inputTitle">Title</label>
                    @error('title')
                
                    <samp class="small text-danger">{{ $message }}</samp>
                    @enderror

                    {{-- <samp class="small text-danger">error</samp> --}}
                </div>
                <div class="form-floating mt-3">
                    <textarea
                     class="form-control" name="description" id="inputDescription" placeholder="Description" >
                     {{ old('description')}}
                    </textarea>
                    <label for="inputDescription">Description</label>
                    @error('description')
                
                    <samp class="small text-danger">{{ $message }}</samp>
                    @enderror
                </div>

                <div class="form-floating mb-3 mt-3">
                    <input class="form-control" name="price" id="inputPrice" type="number" placeholder="Enter price" value="{{ old('price')}}">
                    <label for="inputPrice">Price</label>
                    @error('price')
                
                    <samp class="small text-danger">{{ $message }}</samp>
                    @enderror

                
                </div>
                <div class="form-floating mb-3 mt-3">
                    <input class="form-control" name="qty" id="qty" type="number" placeholder="Enter Quantity" value="{{ old('qty')}}">
                    <label for="qty">Quantity</label>
                    @error('qty')
                
                    <samp class="small text-danger">{{ $message }}</samp>
                    @enderror

                
                </div>
               


                <div class="form-floating mb-3 mt-3">
                    <input class="form-control" name="image" id="inputImage" type="file">
                    <label for="inputImage">Image</label>
                </div>
                <div class="mt-4 mb-0">
                   <button type="submit" class="btn btn-primary">
                        Save
                   </button>
                </div>
            </form>
        </div>
        <div class="card-footer text-center py-3">
            <div class="small"><a href="login.html">Have an account? Go to login</a></div>
        </div>
    </div>


</x-backend.layouts.master>