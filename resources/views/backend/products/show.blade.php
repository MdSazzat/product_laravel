<x-backend.layouts.master>
    <x-slot name="pageTitle">
    Product Details
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">   Product Details </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Add New</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>


    <div class="card shadow-lg border-0 rounded-lg mt-5">
       
            <div class="card-header">
                <i class="fas fa-table me-1"></i> 
                Products <a class="btn btn-primary" href="{{ route('products.index') }}">List</a>
            </div>
            

        <div class="card-body">

            <table  id="datatablesSimple">
<tr>
    <th>Title</th>
    <th>Description</th>
    <th>Price</th>
    <th>Quantity</th>
    <th>Created_at</th>
    <th>Updated_at</th>
</tr>
<tr>
    <td>{{  $product->title }}</td>
    <td>{{ $product->description }}</td>
    <td>{{ $product->price }}</td>
    <td>{{ $product->qty }}</td>
    <td>{{ $product->created_at }}</td>
    <td>{{ $product->updated_at }}</td>
</tr>


            </table>
            {{-- <p>Title:{{  $product->title }}</p>
            <p>Description:{{ $product->description }}</p>
            <p>Price:{{ $product->price }}</p>
            <p>Quantity:{{ $product->qty }}</p> --}}

        </div>


</x-backend.layouts.master>