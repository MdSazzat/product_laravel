<?php

use App\Http\Controllers\ProductController;

use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;
 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('welcome');
});

// Route::get('/home', function(){
//     return view('backend.home');
// });

Route::get('/home', function(){
    return view('backend.home');
});

Route::get('/products', [ProductController::class, 'index'])->name('products.index');

// Route::get('/form', function(){
//     return view('backend.products.form');
// })->name('form');


Route::get('/products/create',[ProductController::class, 'create'])->name('products.create');

// Route::post('/products/{product}',[ProductController::class, 'store'])->name('products.store');
Route::post('/products',[ProductController::class, 'store'])->name('products.store');   
// GET	/photos/{photo}	show	photos.show
Route::get('/products/{product}',[ProductController::class, 'show'])->name('products.show');   

Route::get('/products/{product}/edit',[ProductController::class, 'edit'])->name('products.edit');   
Route::patch('/products/{product}',[ProductController::class, 'update'])->name('products.update');   

Route::delete('/products/{product}',[ProductController::class, 'destroy'])->name('products.destroy');   