<x-backend.layouts.master>
    <x-slot name="pageTitle">
      Edit Product
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Products </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Edit</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>


    <div class="card shadow-lg border-0 rounded-lg mt-5">
       
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
               Edit product <a class="btn btn-primary" href="{{ route('products.index') }}">List</a>
            </div>
            
       
        <div class="card-body">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
          
            <form action="{{ route('products.update', ['product'=>$product->id]) }}"
                 method="POST">
                 @csrf
                 @method('patch')
                
              
                <div class="form-floating mb-3 mb-md-0">
                    <input class="form-control" name="title" id="inputTitle" type="text" placeholder="Enter Title" value="{{ old('title', $product->title)}}">
                    <label for="inputTitle">Title</label>
                    @error('title')
                
                    <samp class="small text-danger">{{ $message }}</samp>
                    @enderror

                    {{-- <samp class="small text-danger">error</samp> --}}
                </div>
                <div class="form-floating mt-3">
                    <textarea
                     class="form-control" name="description" id="inputDescription" placeholder="Description" >
                     {{ old('description',$product->description)}}
                    </textarea>
                    <label for="inputDescription">Description</label>
                    @error('description')
                
                    <samp class="small text-danger">{{ $message }}</samp>
                    @enderror
                </div>
                <div class="form-floating mb-3 mt-3">
                    <input class="form-control" name="image" id="inputImage" type="file">
                    <label for="inputImage">Image</label>
                </div>
                <div class="mt-4 mb-0">
                   <button type="submit" class="btn btn-primary">
                        Save
                   </button>
                </div>
            </form>
        </div>
        <div class="card-footer text-center py-3">
            <div class="small"><a href="login.html">Have an account? Go to login</a></div>
        </div>
    </div>


</x-backend.layouts.master>