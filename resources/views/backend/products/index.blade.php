<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Product
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Product </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Product</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    <!-- <h1 class="mt-4">Tables</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Tables</li>
        </ol> -->
    
    {{-- <div class="card mb-4">
        <div class="card-body">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the
            <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>
            .
        </div>
    </div> --}}

    <div class="card mb-4">
        <div class="card-header">
           
            <a class="btn btn-primary" href="{{ route('products.create') }}">Add New</a>
        </div>
        <div class="card-body">
            @if (session('message'))
            <div class="alert alert-success">
                <span class="close" data-dismiss="alert">&times;</span>
                <strong>{{ session('message') }}.</strong>
            </div>  
            @endif

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>#Sl</th>
                        <th>Title</th>
                        <th>Description</th>
  
                        <th>price</th>
                        <th>Quantity</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                    </tr>
                </tfoot>
                <tbody>
                    @php
                        $sl=0;
                        // dd($products);
                    @endphp
                    @foreach ($products as $product)
                   
                    <tr>  
                      
                        
                        <td>{{ ++$sl}}</td>
                        <td>{{ $product -> title }}</td>
                        <td>{{ $product -> description }}</td>
                        <td>{{ $product -> price }}</td>
                        <td>{{ $product -> qty }}</td>

                        <td><a class="btn btn-primary" href="{{ route('products.show', ['product'=>$product->id]) }}">show</a>
                       
                            <a class="btn btn-warning" href="{{ route('products.edit', ['product'=>$product->id]) }}">Edit</a>
                            {{-- <a class="btn btn-primary" href="{{ route('products.destroy', ['product'=>$product->id]) }}">Delete</a> --}}
                          
                        <form style="display: inline" action="{{ route('products.destroy', ['product'=>$product->id]) }}"
                            method="post">
                            @csrf
                            @method('delete')
                        
                            <button class="btn btn-danger"  onclick="return confirm('Are you sure want to delete ?')" type="submit">Delete</button>
                        </form>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</x-backend.layouts.master>
