<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::latest()->get();
        return view('backend.products.index', [
            'products' => $products
        ]);
    }

    public function create()
    {
        return view('backend.products.create');
    }

    public function store(Request $request)
    
    { 

        try{
            $request->validate([
                'title' => 'required|min:3',
               
                // 'title' => 'required|min:3',

                'description' => 'required|min:10',
               
                //    'price'=>'required |numeric'
             
                // 'title' => ['required', 'min:3']
            ]);
            // dd(request()->all());
            Product::create([
                'title'=>$request->title,
                'description'=>$request->description,
                'price'=>$request->price,
                'qty'=>$request->qty,

    
              
            ]);
            // $request->session()->flash('message','Task is Successfully');
            return redirect()->route('products.index')->withMessage('Successfully Created');  


        }catch(QueryException $e){
 
         return redirect()->back()->withInput()->withErrors($e->getMessage());

        //   dd($e->getMessage());

        }
       
    }


    public function show(Product $product){
        // $product= Product::findOrFail($product);  
        return view('backend.products.show',['product'=>$product]);
        // dd($product);
        


    }

    public function edit(Product $product){
        // $product= Product::findOrFail($product);  
        return view('backend.products.edit',['product'=>$product]);
        // dd($product);
        


    }

    public function update(Request $request, Product $product){
        try{
            $request->validate([
                'title' => 'required|min:3',
                'description' => 'required|min:10'
             
                // 'title' => ['required', 'min:3']
            ]);
            // dd(request()->all());
            $product->update([
                'title'=>$request->title,
                'description'=>$request->description,
    
              
            ]);
            // $request->session()->flash('message','Task is Successfully');
            return redirect()->route('products.index')->withMessage('Successfully updated !');  


        }catch(QueryException $e){
 
         return redirect()->back()->withInput()->withErrors($e->getMessage());

        //   dd($e->getMessage());

        }

    }


    public function destroy(Product $product){
        try{
        $product->delete();
        return redirect()->route('products.index')->withMessage('Successfully deleted !');  

        }catch(QueryException $e){
           
         return redirect()->back()->withInput()->withErrors($e->getMessage()); 
        }
    }
}
