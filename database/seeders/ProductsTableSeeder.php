<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Product::create([
            'title' => 'Fashion',
            'description' => 'Fashion Description',
        ]);

        \App\Models\Product::create([
            'title' => 'Child',
            'description' => 'Child Fashion',
        ]);
        
        \App\Models\Product::create([
            'title' => 'Men',
            'description' => 'Men Fashion',
        ]);
    }
}
